Source: python-line-profiler
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: python
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               dpkg-dev (>= 1.17.14),
               python3-all-dev,
               python3-ipython <!nocheck>,
               python3-pytest <!nocheck>,
               python3-setuptools
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/python-line-profiler
Vcs-Git: https://salsa.debian.org/science-team/python-line-profiler.git
Homepage: https://github.com/pyutils/line_profiler

Package: python3-line-profiler
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: line-by-line profiling for Python
 LineProfiler can be given functions to profile, and it will time the execution
 of each individual line inside those functions. In a typical workflow, one
 only cares about line timings of a few functions because wading through the
 results of timing every single line of code would be overwhelming. However,
 LineProfiler does need to be explicitly told what functions to profile.
 .
 If you are using IPython, there is an implementation of an %lprun magic
 command which will let you specify functions to profile and a statement to
 execute.
